package com.n6consulting.m2m.welcome;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Example {

  @RequestMapping("/welcome")
  String home() {
    return "Hello World!";
  }
}
